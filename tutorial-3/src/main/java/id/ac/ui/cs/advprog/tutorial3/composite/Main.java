package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

public class Main {
    public static void main(String[] args) {
        Company theCompany = new Company();

        BackendProgrammer backend = new BackendProgrammer("Alice", 100000);
        theCompany.addEmployee(backend);

        FrontendProgrammer frontend = new FrontendProgrammer("Bob", 95000);
        theCompany.addEmployee(frontend);

        Ceo ceo = new Ceo("Cecile", 500000);
        theCompany.addEmployee(ceo);

        Cto cto  = new Cto("Dane", 400000);
        theCompany.addEmployee(cto);

        UiUxDesigner uiux = new UiUxDesigner("Emily", 80000);
        theCompany.addEmployee(uiux);

        NetworkExpert network = new NetworkExpert("Frans", 90000);
        theCompany.addEmployee(network);

        SecurityExpert security = new SecurityExpert("Giovanni", 110000);
        theCompany.addEmployee(security);

        System.out.println("The Company total salaries: " + theCompany.getNetSalaries());
        System.out.println("The Company employees number: " + theCompany.getAllEmployees().size());
        System.out.println("The Company employees name:");
        for (Employee employee : theCompany.getAllEmployees()) {
            System.out.println("- " + employee.getName());
        }
    }
}
