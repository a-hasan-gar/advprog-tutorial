package id.ac.ui.cs.advprog.tutorial3.composite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Company {
    protected List<Employee> employeeList;

    public Company() {
        employeeList = new ArrayList<Employee>();
    }

    public Company(List<Employee> employeeList) {
        Collections.copy(this.employeeList, employeeList);
    }

    public void addEmployee(Employee employee) {
        employeeList.add(employee);
    }

    public double getNetSalaries() {
        double totalSalaries = 0.0;

        for (Employee employee : employeeList) {
            totalSalaries += employee.getSalary();
        }

        return totalSalaries;
    }

    public List<Employee> getAllEmployees() {
        return employeeList;
    }
}
