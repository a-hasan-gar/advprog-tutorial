package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employee;

public class FrontendProgrammer extends Employee {

    public FrontendProgrammer(String name, double salary) {
        if (salary < 30000.0) {
            throw new IllegalArgumentException();
        }

        this.name = name;
        this.salary = salary;
        role = "Front End Programmer";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
