package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class Main {
    public static void main(String[] args) {
        // Thick bread burger with beef meat, cheese, and tomato sauce
        Food cheeseBurgerXl = BreadProducer.THICK_BUN.createBreadToBeFilled();
        cheeseBurgerXl = FillingDecorator.BEEF_MEAT.addFillingToBread(cheeseBurgerXl);
        cheeseBurgerXl = FillingDecorator.CHEESE.addFillingToBread(cheeseBurgerXl);
        cheeseBurgerXl = FillingDecorator.TOMATO_SAUCE.addFillingToBread(cheeseBurgerXl);

        System.out.println(cheeseBurgerXl.getDescription());
        System.out.println("Cost: $" + cheeseBurgerXl.cost());
    }
}
