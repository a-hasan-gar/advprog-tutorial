package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory;

public class DepokSpecialSauceCheesePizza extends Pizza {
    PizzaIngredientFactory ingredientFactory;

    public DepokSpecialSauceCheesePizza(PizzaIngredientFactory ingredientFactory) {
        this.ingredientFactory = ingredientFactory;
    }

    @Override
    public void prepare() {
        System.out.println("Preparing " + name);
        dough = ingredientFactory.createDough();
        cheese = ingredientFactory.createCheese();
        veggies = ingredientFactory.createVeggies();
        sauce = ingredientFactory.createSauce();
    }
}
