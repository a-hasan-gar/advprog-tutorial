package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory;

public class DepokCheesePizza extends Pizza {
    PizzaIngredientFactory ingredientFactory;

    public DepokCheesePizza(PizzaIngredientFactory ingredientFactory) {
        this.ingredientFactory = ingredientFactory;
    }

    @Override
    public void prepare() {
        System.out.println("Preparing " + name);
        dough = ingredientFactory.createDough();
        cheese = ingredientFactory.createCheese();
        veggies = ingredientFactory.createVeggies();
    }
}
